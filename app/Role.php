<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'users_roles';

    protected $fillable = [
        'id',
        'name',
        'description',
        'updated_at'
    ];
}
