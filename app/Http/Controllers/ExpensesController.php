<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use App\Http\Resources\Expenses as ExpensesResource;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('expenses.expenses');
    }

   public function expensecollection() {
        $expenses = Expense::all();

        return ExpensesResource::collection($expenses);
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $expense = $request->isMethod('put') ? Expense::findOrFail($request->id) : new Expense;

        $expense->id = $request->input('id');
        $expense->category = $request->input('category');
        $expense->amount = $request->input('amount');
        
        if($expense->save())
        {
            return new ExpensesResource($expense);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::findOrFail($id);

        if ($expense->delete()) {
            return new ExpensesResource($expense);
        }
    }
}
