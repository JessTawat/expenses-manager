@extends('layouts.dashboard')
@section('content')
<div>
    <div class="container-fluid">
        <div class="row">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="header">
                                    <h4 class="title">Expenses Category</h4>
                                        <expenses-category />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
