<div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="{{ route('home') }}">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                @can('isAdmin')
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="pe-7s-user"></i>
                        <p>User Management</p>
                    </a>
                    <ul>
                        <li><a href="{{ route('user-roles') }}">Roles</a></li>
                        <li><a href="{{ route('users') }}">Users</a></li>
                    </ul>
                </li>
                @endcan
                
                <li>
                    <a href="#">
                        <i class="pe-7s-piggy"></i>
                        <p>Expenses Management</p>
                    </a>
                    <ul>
                        @can('isAdmin')
                        <li><a href="{{ route('expenses-category') }}">Expenses Category</a></li>
                        @endcan
                        <li><a href="{{ route('expenses') }}">Expenses</a></li>
                    </ul>
                </li>
            </ul>
        </div>
</div>