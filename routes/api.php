<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// role collection
Route::get('roles', 'RoleController@getrolecollection');
// store role
Route::post('role', 'RoleController@store');
// update role
Route::put('role', 'RoleController@store');
// delete role
Route::delete('role/{id}', 'RoleController@destroy');


// User Collection
Route::get('users', 'UserController@getuserscollection');
// Store User
Route::post('user', 'UserController@store');
// Update User
Route::put('user', 'UserController@store');
// delete User
Route::delete('user/{id}', 'UserController@destroy');

// category collection
Route::get('categories', 'ExpensesCategoryController@categorycollection');
// Store Category
Route::post('category', 'ExpensesCategoryController@store');
// update Category
Route::put('category', 'ExpensesCategoryController@store');
// delete User
Route::delete('category/{id}', 'ExpensesCategoryController@destroy');
//get chart category
Route::get('chart-category', 'ExpensesCategoryController@getcategory');


// expenses collection
Route::get('expenses', 'ExpensesController@expensecollection');
// Store expenses
Route::post('expense', 'ExpensesController@store');
// update expenses
Route::put('expense', 'ExpensesController@store');
// delete Uexpensesser
Route::delete('expense/{id}', 'ExpensesController@destroy');

